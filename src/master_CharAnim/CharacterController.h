#ifndef CHARACTERCONTROLLER_H
#define CHARACTERCONTROLLER_H

#include <mat.h>
#include "FiniteStateMachine.h"





class CharacterController

    {
    public:
        CharacterController();
    

        void update(const float dt);

        void turnXZ(const float& rot_angle_v);
        void sauter();
        void kick();
        void accelerate(const float& speed_inc);
        void setVelocityMax(const float vmax);
        FiniteStateMachine& fsm() { return m_fsm; }

        const Point position() const;
        const Vector direction() const;
        float velocity() const;
        void setVelocity(const float v);
        const Transform& controller2world() const { return m_ch2w; }
        bool isJumping() const { return jumping; }
        bool isKicking() const { return kicking; }


    protected:
        Transform m_ch2w;   // matrice du character vers le monde
                            // le personnage se déplace vers X
                            // il tourne autour de Y
                            // Z est sa direction droite
                                    
        float m_v;          // le vecteur vitesse est m_v * m_ch2w * Vector(1,0,0)
        float m_vMax;       // ne peut pas accélérer plus que m_vMax
        FiniteStateMachine m_fsm;
        bool jumping, kicking;
        float kickTotalTime = 0;
        float kickClock = 0;
    };





    #endif