# TP d'Animation de Personnage (M1)

Ce projet de Master 1 porte sur l'animation de personnage. Il consiste à développer un module en C++ permettant la gestion et l'animation d'un squelette de personnage à partir de fichiers BVH. Le projet est divisé en quatre parties principales.

## TP Partie 1 : Affichage

### Objectifs
Créer un module `Skeleton` capable de stocker et manipuler un squelette composé de plusieurs articulations. Chaque articulation stocke l'identifiant de l'articulation parent et une matrice de transformation du repère de l'articulation vers le repère du monde.

### réalisé
- Déclarer la classe `Skeleton` avec ses méthodes pour initialiser et manipuler les articulations.
- Utiliser le fichier BVH pour tester.
- Implémenter les fonctions d'affichage du squelette dans le viewer.

## TP Partie 2 : Contrôleur d'Animation

### réalisé
Développer un contrôleur de personnage `CharacterController` qui permet de manipuler le déplacement d'un personnage via le clavier.
Implémentation  du contrôle de base pour déplacer une sphère.
utiliser "Z", "Q", "S", "D" pour les mouvements, "E" pour le kick et " " pour le saut auquel j'ai attribué l'animation du back_flip
Ajout des animations de personnage (marche, course, coup de pied) et changer les animations en fonction des entrées clavier.

## TP Partie 3 : Transition et Graphe d'Animation

### Objectifs
Améliorer le réalisme des transitions entre animations et construire un graphe d'animation pour automatiser les transitions.



## TP Partie 4 : Interaction entre le Personnage et des Sphères Physiques

### Objectifs
Gérer les interactions physiques entre le personnage et des objets sphériques dans l'environnement.

### réalisation
- Implémenter le comptage et l'affichage des particules.
- collisions des sphères avec le sol, utilisation du rebond pour simuler le réalisme des balles en tombant.

## Compilation et Exécution

sous visual studio :

lancer :
/premake/premake.bat

ensuite :
/premake/premake-CharA.bat

aller dans /build
ouvrir /build/gKit2light.sln et ensuite lancer le projet 

