#include "FiniteStateMachine.h"
#include "CharacterController.h"

#include <iostream>
#include "window.h"







FiniteStateMachine::FiniteStateMachine(const CharacterController* controller) : currentState(WALKING), controller(controller) {
    // Load BVH files
    createNode(WAITING, "data/bvh/motionFSM/avatar_smoke_idle.bvh");
    createNode(WALKING, "data/bvh/motionFSM/avatar_walk.bvh");
    createNode(RUNNING, "data/bvh/motionFSM/avatar_run.bvh");
    createNode(KICKING, "data/bvh/motionFSM/avatar_kick_roundhouse_R.bvh");
    createNode(JUMPING, "data/bvh/motionGraph_second_life/avatar_backflip.bvh");
    //transition(WAITING);
}

void FiniteStateMachine::createNode(AnimationState state, const std::string& animpath) {
    bvh[state].init(smart_path(animpath.c_str()));
}



// Définition de seuils de vitesse comme constantes pour une meilleure clarté et maintenance
const float V_MARCHER = 40.0f;
const float V_COURIR = 80.0f;

AnimationState FiniteStateMachine::findTransition(AnimationState from)
{
    // Vérification de la validité de `controller`
    if (controller == nullptr) {
        std::cerr << "Erreur: controller est null!" << std::endl;
        return NO_TRANSITION;
    }

    switch (from) {

    case WAITING:
        if (controller->velocity() > 0.0f && controller->velocity() <= V_MARCHER)
            return WALKING;
        else if (controller->velocity() > V_COURIR)
            return RUNNING;
        else if (controller->isJumping())
            return JUMPING;
        else if (controller->isKicking())
            return KICKING;
        break;

    case WALKING:
        //std::cout << "Vitesse : " << controller->velocity() << std::endl;
        if (controller->velocity() > V_COURIR)
            return RUNNING;
        else if (controller->velocity() == 0.0f)
            return WAITING;
        else if (controller->isJumping())
            return JUMPING;
        break;

    case RUNNING:
        if (controller->velocity() == 0.0f)
            return WAITING;
        else if (controller->velocity() <= V_COURIR)
            return WALKING;
        break;

    case JUMPING:
        if (!controller->isJumping())
            return WAITING;
        break;

    case KICKING:
        if (!controller->isKicking())
            return WAITING;
        break;



        /*
    case KICKING :
        if (controller->velocity() > 0.0f && controller->velocity() <= SPEED_THRESHOLD_RUNNING_TO_WALKING)
            return WALKING;
        else if (controller->velocity() == 0.0f)
            return WAITING;
        else if (controller->isJumping())
            return JUMPING;
        break;*/


    default:
        // Peut-être loguer un avertissement ou une erreur si l'état n'est pas géré
        std::cerr << "Avertissement: État non pris en charge." << std::endl;
        break;
    }

    return NO_TRANSITION;
}

const chara::BVH& FiniteStateMachine::getAnim(AnimationState anim) const
{
    return bvh.at(anim);

}


void FiniteStateMachine::update(float deltaTime)
    {
        //std::cout << "État actuel : " << currentState << ", vitesse : " << controller->velocity() << std::endl;

        // On met à jour le temps qui passe
        time += deltaTime;
        if (time > 6.0f) {  
            time = 0.f;
        }
        // Check si transi possible

        AnimationState nextState = findTransition(currentState);

        if (nextState != NO_TRANSITION)
        {
            transition(nextState);
        }
    }


void FiniteStateMachine::transition(AnimationState nextState) {
    std::cout << "Transition de " << currentState << " à " << nextState << std::endl;
    currentState = nextState;
    time = 0.f;
}


const chara::BVH& FiniteStateMachine::getCurrentAnim() const { return bvh.at(currentState); }


int FiniteStateMachine::getCurrentAnimFrame() const {
    const chara::BVH& currentAnim = getCurrentAnim();
    int totalFrames = currentAnim.getNumberOfFrame();
    float frameTime = currentAnim.getFrameTime();
    float totalDuration = frameTime * totalFrames;

    //std::cout << "Total frames: " << totalFrames << ", Frame time: " << frameTime << ", Total duration: " << totalDuration << std::endl;

    float currentTimeInAnimation = std::fmod(time, totalDuration);
    int currentFrameIndex = static_cast<int>((currentTimeInAnimation / totalDuration) * totalFrames);

    /*     if (currentFrameIndex >= totalFrames) {
        currentFrameIndex = totalFrames - 1;
    } */

    //std::cout << "Current time: " << time << ", Current time in Animation: " << currentTimeInAnimation << ", Current Frame Index: " << currentFrameIndex << std::endl;

    return currentFrameIndex;
}




