
#include "Skeleton.h"

using namespace chara;

void Skeleton::init(const BVH& bvh)
{
    // TODO
    SkeletonJoint sj;
    for (int i = 0; i < bvh.getNumberOfJoint(); i++)
    {
        sj.m_parentId = bvh.getJoint(i).getParentId();
        m_joints.push_back(sj);
    }


}


Point Skeleton::getJointPosition(int i) const
{
    // TODO
    return m_joints[i].m_l2w(Point(0, 0, 0));
}


int Skeleton::getParentId(const int i) const
{
    // TODO
    return m_joints[i].m_parentId;
}

//! Positionne ce squelette dans la position n du BVH.
   //! Assez proche de la fonction r�cursive (question 1), mais range la matrice (Transform)
   //! dans la case du tableau. Pour obtenir la matrice allant de l'articulation local vers le monde,
   //! il faut multiplier la matrice allant de l'articulation vers son p�re � la matrice du p�re allant de
   //! l'articulation du p�re vers le monde.



void Skeleton::setPose(const BVH& bvh, int frameNumber)
{
        // TODO
    // Parcourir toutes les articulations (SkeletonJoint ou BVHJoint) 
    //     Declarer la matrice l2f (pere<-local)
    //     Init avec la teanslation Sffset
    //     Parcourir tous les channels
    //          Accumuler dans la matrice l2f les translations et rotation en fonction du type de Channel
    // Multiplier la matrice l2f avec la matrice l2w (world<-local) du p�re qui est d�j� stock� dans le tableau 
    // Attention il peut ne pas y avoir de p�re (pour la racine)
    for (int i = 0; i < bvh.getNumberOfJoint(); i++)
    {
        const BVHJoint& joint = bvh.getJoint(i);
        float x, y, z;
        joint.getOffset(x, y, z);
        
        // Initialize with the translation offset
        Transform l2f = Translation(x, y, z);

        // Process each channel for this joint in this frame
        for (int channelIndex = 0; channelIndex < joint.getNumberOfChannel(); ++channelIndex)
        {
            const BVHChannel& channel = joint.getChannel(channelIndex);
            float value = channel.getData(frameNumber);

            // Apply the translation or rotation based on the channel type
            if (channel.getType() == BVHChannel::TYPE_ROTATION)
            {
                if (channel.getAxis() == AXIS_X) // X axis
                    l2f = l2f * RotationX(value);
                else if (channel.getAxis() == AXIS_Y) // Y axis
                    l2f = l2f * RotationY(value);
                else if (channel.getAxis() == AXIS_Z) // Z axis
                    l2f = l2f * RotationZ(value);
                else
                    std::cerr << "Invalid axis" << std::endl;
            }
            else if (channel.getType() == BVHChannel::TYPE_TRANSLATION)
            {
                if (channel.getAxis() == AXIS_X) // X axis
                    l2f = l2f * Translation(value, 0.0, 0.0);
                else if (channel.getAxis() == AXIS_Y) // Y axis
                    l2f = l2f * Translation(0.0, value, 0.0);
                else if (channel.getAxis() == AXIS_Z) // Z axis
                    l2f = l2f * Translation(0.0, 0.0, value);
                else
                    std::cerr << "Invalid axis" << std::endl;
                
            }
        }

        // Combine with parent's world transformation
        if (joint.getParentId() >= 0)
        {
            m_joints[i].m_l2w = m_joints[joint.getParentId()].m_l2w * l2f;
        }
        else
        {
            // For the root joint
            m_joints[i].m_l2w = l2f;
        }
    }
}
