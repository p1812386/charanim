
#include <cassert>
#include <cmath>
#include <cstdio>
#include <iostream>

#include "CharAnimViewer.h"

using namespace std;
using namespace chara;


CharAnimViewer* CharAnimViewer::psingleton = NULL;


CharAnimViewer::CharAnimViewer() : Viewer(), m_frameNumber(0)
{
	psingleton = this;
}


int CharAnimViewer::init()
{
    Viewer::init();
    cout<<"==>master_CharAnim/CharAnimViewer"<<endl;
    m_camera.lookat( Point(0,0,0), 1000 );
	m_camera.rotation(180, 0);
    gl.light( Point(300, 300, 300 ) );

    //b_draw_grid = false;

    m_world.setParticlesCount( 10 );


    init_cylinder();
    init_sphere();
	


    m_bvh.init( smart_path("data/bvh/motionFSM/avatar_smoke_idle.bvh") );
	//m_bvh.init( smart_path("data/bvh/danse.bvh") );

    m_frameNumber = 0;
    cout<<endl<<"========================"<<endl;
    cout<<"BVH decription"<<endl;
    cout<<m_bvh<<endl;
    cout<<endl<<"========================"<<endl;

    m_ske.init( m_bvh );
    m_ske.setPose( m_bvh, 0);// met le skeleton a la pose au repos

    return 0;
}


void CharAnimViewer::checkCollisionWithSkeleton() {

    Transform f2w = charController.controller2world() * RotationY(90.f);
    // TODO
    for (int i = 0; i < m_ske.numberOfJoint(); i++)
    {
        Point p = f2w(m_ske.getJointPosition(i));
        m_world.collision(p, 2);
    }

    
}


void CharAnimViewer::draw_skeleton(const Skeleton& ske)
{
    Transform f2w = charController.controller2world() * RotationY(90.f);
    // TODO
    for(int i = 0; i < ske.numberOfJoint(); i++)
    {
        int parentId = ske.getParentId(i);
        Point p = f2w(ske.getJointPosition(i));

        if (parentId < 0)
        {
            draw_sphere(p, 2);
        }
        else
        {
            Point p2 = f2w(ske.getJointPosition(parentId));
            draw_sphere(p, 2);
            draw_cylinder(p, p2, 1);
        }

    }

    //draw_sphere(charController.position(), 5);


}



int CharAnimViewer::render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //draw_quad( RotationX(-90)*Scale(500,500,1) );

    Viewer::manageCameraLight();
    gl.camera(m_camera);

	// Affiche les particules physiques (Question 3 : interaction personnage sphere/ballon)
    m_world.draw();


	// Affiche le skeleton � partir de la structure lin�aire (tableau) Skeleton
    draw_skeleton( m_ske );



    return 1;
}


int CharAnimViewer::update( const float time, const float delta )
{
    // time est le temps ecoule depuis le demarrage de l'application, en millisecondes,
    // delta est le temps ecoule depuis l'affichage de la derniere image / le dernier appel a draw(), en millisecondes.

	if (key_state('n')) { m_frameNumber++; cout << m_frameNumber << endl; }
	if (key_state('b')) { m_frameNumber--; cout << m_frameNumber << endl; }
    if (m_frameNumber < 0)
    {
        m_frameNumber += m_bvh.getNumberOfFrame();
    }

    m_frameNumber %= m_bvh.getNumberOfFrame();

    // Ajoutez un membre de classe pour suivre l'état de la touche 'z'
    bool m_zKeyDown = false;
    if (key_state('z')) 
    { 
        charController.accelerate(250.0f * (delta / 1000.0f ));
        m_zKeyDown = true;
    }
    else { m_zKeyDown = false;}
    if (key_state('q')) { charController.turnXZ(200.0f * (delta / 1000.0f)); }
    if (key_state('d')) { charController.turnXZ(-200.0f * (delta / 1000.0f)); }
    if (key_state('s')) { charController.accelerate(-250.0f * (delta / 1000.0f)); }
    if (key_state(' ')) { charController.sauter();}

    if (key_state('e') && !charController.isKicking()) { charController.kick(); }


    charController.update(delta / 1000.0f);

    m_ske.setPose(charController.fsm().getCurrentAnim(), charController.fsm().getCurrentAnimFrame() );

    m_world.update(0.1f);
    checkCollisionWithSkeleton();

    return 0;
}



