#ifndef FINITESTATEMACHINE_H
#define FINITESTATEMACHINE_H

#include <unordered_map>
#include <BVH.h>

enum AnimationState {
    NO_TRANSITION = -1,
    WAITING = 0,
    WALKING,
    RUNNING,
    JUMPING,
    KICKING,
    ANIMATION_COUNT
};

class CharacterController;

class FiniteStateMachine {
private:
    AnimationState  currentState;
    const CharacterController* controller;
    std::unordered_map<AnimationState, chara::BVH> bvh;

    float time = 0.f;


public:
    FiniteStateMachine(const CharacterController* controller);

    void createNode(AnimationState state, const std::string& animpath);

    AnimationState findTransition(AnimationState from);

    const chara::BVH& getAnim(AnimationState anim) const;

    void update(float deltaTime);

    void transition(AnimationState nextState);
    
    const chara::BVH& getCurrentAnim() const;

    int getCurrentAnimFrame() const;  
};

#endif