#include "CharacterController.h"



CharacterController::CharacterController() : m_fsm(this){
    m_v = 0;
    m_vMax = 100.0;
    m_ch2w = Identity();
    jumping = false, kicking = false;
    kickTotalTime = m_fsm.getAnim(KICKING).getFrameTime() * m_fsm.getAnim(KICKING).getNumberOfFrame();

}

void CharacterController::update(const float dt) {
    if (isKicking()) {
        kickClock += dt;
        if (kickClock > kickTotalTime)
        {
            kicking = false;
        }
    }
    m_ch2w = Translation(m_v * direction() * dt) * m_ch2w;
    //simule la gravite, dans le cas ou on saute ca nous fazit bien redescendre
    if(m_ch2w(vec4(0, 0, 0, 1)).y > 0.01) {
        m_ch2w = Translation(0, -1, 0) * m_ch2w;
    }
    else {
        m_ch2w = Translation(0, - m_ch2w(vec4(0, 0, 0, 1)).y, 0) * m_ch2w;
        jumping = false;
    }

    m_fsm.update(dt);

}

void CharacterController::turnXZ(const float& rot_angle_v) {
    
    m_ch2w = m_ch2w * RotationY(rot_angle_v);
}

void CharacterController::sauter() {
    
        m_ch2w = Translation(0, 3, 0) * m_ch2w ;
        jumping = true;
}

void CharacterController::kick() {
    if (velocity() > 0.0f) return;
    kicking = true;
    kickClock = 0.0f;
}

void CharacterController::accelerate(const float& speed_inc) {
    this->m_v += speed_inc;
    if (m_v < 0.01) m_v = 0;
    if (m_v > m_vMax) m_v = m_vMax;
    std::cout << this->m_v << std::endl;

}


void CharacterController::setVelocityMax(const float vmax) {
    m_vMax = vmax;
}

void CharacterController::setVelocity(const float v) {
    m_v = v;
}

const Point CharacterController::position() const {
    return m_ch2w(Point(0,0,0));
}

const Vector CharacterController::direction() const {
    return normalize(m_ch2w(Vector(1,0,0)));
}


float CharacterController::velocity() const {
    return m_v;
}

